class CarLogsController < ApplicationController
	def home
		@CarLog = CarLog.last 
	end
	def new 
		@CarLog = CarLog.new
	end
	def create
		@CarLog = CarLog.new(carlog_params)
		@CarLog.save
		

		if@CarLog.save
			redirect_to car_log_path(@CarLog)
		else
			render 'new'	
		end
	end
	
	def creator
	end
	def show
		if CarLog.all.count.zero?
			flash[:notice] = "No Cars have been registered yet!"
			render 'home'
		else
		@CarLog = CarLog.last
	end
	end
	private
		def carlog_params
			params.require(:car_log).permit(:time_in, :time_out, :plate_number, :color, :brand)


		end
	
end
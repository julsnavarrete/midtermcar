Rails.application.routes.draw do
	root to: 'car_logs#home'
	get 'car_logs/home', to: 'car_logs#home'
	get 'car_logs/creator', to: 'car_logs#creator'
	get 'car_logs/new', to: 'car_logs#new'
	get 'car_logs/show', to: 'car_logs#show'

	resources :car_logs

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
